package util.test;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;

import com.hm.common.network.httpclient.HttpClientFactory;
import com.hm.common.network.httpclient.HttpClientStatus;
import com.hm.common.util.RandomUtil;

/**
 * @author shishun.wang
 * @date 2018年5月3日 下午12:00:58
 * @version 1.0
 * @describe
 */
public class AdTest {

	private static void test() throws Exception {
		// 在线程可以通过await（）之前必须调用countDown（）的次数
		// 具有计数1的 CountdownLatch 可以用作”启动大门”，来立即启动一组线程；
		final CountDownLatch begin = new CountDownLatch(1); // 为0时开始执行
		final ExecutorService exec = Executors.newFixedThreadPool(5);
		for (int i = 0; i < 5; i++) {
			final int NO = i + 1;
			Runnable runnable = new Runnable() {
				public void run() {
					try {
						// 如果当前计数为零，则此方法立即返回。
						// 如果当前计数大于零，则当前线程将被禁用以进行线程调度，并处于休眠状态，直至发生两件事情之一：
						// 或调用countDown（）方法，计数达到零;
						// 或一些其他线程中断当前线程。
						// 等待直到 CountDownLatch减到1
						begin.await();

						new AdTest().test2();

					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			};
			exec.submit(runnable);
		}
		long start = System.currentTimeMillis();
		System.out.println("开始执行");
		// 减少锁存器的计数，如果计数达到零，释放所有等待的线程。
		// begin减一，开始并发执行
		begin.countDown();
		// 此方法不等待先前提交的任务完成执行
		// exec.shutdown();
		// 为了保证先前提交的任务完成执行 使用此方法
		exec.awaitTermination(4000, TimeUnit.MILLISECONDS);

		System.out.println("执行完成" + (System.currentTimeMillis() - start));
	}

	public void test2() throws Exception, IOException {
		String uri = "http://localhost:8081/adTest/receiveReward.do";
		Map<String, Object> param = new HashMap<String, Object>();
		{
			param.put("userId", RandomUtil.randomNumber(10000, 1000000));
			param.put("userTagIds", Arrays.asList(25));
			param.put("mobilePhone", RandomUtil.randomNumber(10, 1000000) + "11");
			param.put("cityCode", "028");
			param.put("carTypeLevel", "FAST");
			param.put("carTypeCategory", "FASTCAR");
			param.put("emPlayRuleId", 30);
			param.put("orderId", Math.random());
		}

		HttpResponse response = HttpClientFactory.POST.build(uri).parameters(param).execute();
		System.out.println("请求参数" + param.toString() + "##################" + EntityUtils.toString(response.getEntity(), HttpClientStatus.CHARACTER_ENCODING));
	}

	public static void main(String[] args) throws Exception {

		ExecutorService executorService = Executors.newFixedThreadPool(50);

		for (int i = 0; i < 100; i++) {
			new Thread() {
				public void run() {
					for (int j = 0; j < 500; j++) {
						executorService.execute(new Runnable() {

							@Override
							public void run() {
								try {
									new AdTest().test2();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
					}
				};
			}.start();
		}

		// test();
	}
}
