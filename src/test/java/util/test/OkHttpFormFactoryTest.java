package util.test;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.hm.common.network.okhttp.OkHttpFormFactory;

/**
 * @author shishun.wang
 * @date 2018年7月5日 下午3:42:16
 * @version 1.0
 * @describe
 */
public class OkHttpFormFactoryTest {

	public static void main(String[] args) throws Exception {
		String url = "http://tool.oschina.net/action/format/sql";
		Map<String, String> param = new HashMap<String, String>();
		{
			param.put("sqlType", "mysql");
			param.put("sql", "select * from aa");
		}
		String responseJson = OkHttpFormFactory.POST.build(url, param).execute().body().string();

		Map map = JSON.parseObject(responseJson);
		System.out.println(map.get("fsql"));
	}
}
