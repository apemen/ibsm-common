package util.test.rocketmq.study.sample;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

/**
 * @author shishun.wang
 * @date 2018年4月26日 下午6:08:12
 * @version 1.0
 * @describe
 */
public class MyProducer {

	public static String GROUP = "SSW_GROUP_01";

	public static String TOPIC = "SSW_TOPIC_01";

	public static String TAG = "ssw_tag01";

	public static void main(String[] args) throws Exception {
		DefaultMQProducer producer = new DefaultMQProducer(GROUP);
		producer.setNamesrvAddr("10.28.16.106:9876,10.28.16.157:9876");
		producer.start();

		
		Message message = new Message(TOPIC, TAG, "发送消息".getBytes());
		SendResult result = producer.send(message);
		
		System.out.println(result.toString());
		
		
	}
}
