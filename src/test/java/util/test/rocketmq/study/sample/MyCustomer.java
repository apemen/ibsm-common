package util.test.rocketmq.study.sample;

import java.util.List;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;

/**
 * @author shishun.wang
 * @date 2018年4月26日 下午6:21:12
 * @version 1.0
 * @describe
 */
public class MyCustomer {

	public static String GROUP = "SSW_GROUP_01";

	public static String TOPIC = "SSW_TOPIC_01";

	public static String TAG = "ssw_tag01";

	public static void main(String[] args) throws Exception {

		DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(GROUP);
		consumer.setNamesrvAddr("10.28.16.106:9876,10.28.16.157:9876");

		{
			consumer.subscribe(TOPIC, TAG);
			consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
			consumer.registerMessageListener(new MessageListenerConcurrently() {

				@Override
				public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
					// TODO Auto-generated method stub
					return null;
				}
				
			});
		}
		
		
		consumer.start();
	}
}
