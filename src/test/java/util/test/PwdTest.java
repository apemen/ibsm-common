package util.test;

import com.hm.common.util.EncryptUtil;

/**
 * @author shishun.wang
 * @date 2018年4月23日 下午12:07:01
 * @version 1.0
 * @describe 
 */
public class PwdTest {

	public static void main(String[] args) {
		
		System.out.println(EncryptUtil.AES.decrypt("920141A9EDD3CD500DAA6DA6752E1B37"));
		System.out.println(EncryptUtil.AES.encrypt("root"));
		System.out.println(EncryptUtil.AES.encrypt("2QMQPS^fTsL9q81_mS7,32"));
		System.out.println(EncryptUtil.Md5.getMD5Code("Gzg3kRerPjI7MRqw5NGF"));
		
	}
}
