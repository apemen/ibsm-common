package util.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.hm.common.util.DateUtil;

/**
 * @author shishun.wang
 * @date 下午7:12:46 2017年10月17日
 * @version 1.0
 * @describe 
 */
public class Test123 {

	public static void main(String[] args) throws ParseException {
//		String[] collections = { "obdLoginDoc", "obdAlarmDescDoc", "obdAlarmUpsideDoc", "obdBaseStationDoc",
//				"obdCarsUpsideDoc", "obdDormancyAwakenDoc", "obdDormancyEnterDoc", "obdDrivingBehaviorUpsideDoc",
//				"obdMalfunctionCodeDoc" };
//		for (String string : collections) {
//			System.out.println("db."+string+".remove({'createTime' : {$lt:ISODate('2017-10-17T11:16:35.639Z')}});");
//		}
		
		System.out.println(DateUtil.toIosDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2018-04-12 00:00:00")));
		System.out.println(DateUtil.toIosDate(new Date()));
		

		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(DateUtil.isoDate("2018-02-12T16:00:00.000Z")));
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(DateUtil.isoDate("2018-02-13T15:59:59.000Z")));
	}
}
