package util.test;

/**
 * @author shishun.wang
 * @date 2017年7月5日 下午5:06:47
 * @version 1.0
 * @describe 
 */
public enum UpdateLogStatusEnum {

	FINISHED(0, "已完成"),

	FAILURE(1, "失败"),

	PROCESSING(2, "处理中"),

	DELETE(3, "已删除");

	private UpdateLogStatusEnum(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	private Integer code;

	private String msg;

	public int value() {
		return this.code;
	}

	public String desc() {
		return this.msg;
	}
	
	public static UpdateLogStatusEnum trance(int code){
		for(UpdateLogStatusEnum status:UpdateLogStatusEnum.values()){
			if(status.code == code){
				return status;
			}
		}
		return null;
	}
}
