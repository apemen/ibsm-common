package util.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.hm.common.util.DateUtil;

/**
 * @author shishun.wang
 * @date 2018年4月25日 下午6:12:00
 * @version 1.0
 * @describe 
 */
public class AATest {

	public static void main(String[] args) throws Exception {
		String str = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
		
		System.out.println(replaceBlank(str));
		
		
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2018-04-20 14:03:25"));
	}
	
	  /**
     * 去掉换行符 James
     *
     * @param str
     * @return
     */
    public static String replaceBlank(String str) {
        String dest = "";
        if (str != null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }
}
