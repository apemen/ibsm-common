package util.test;

import org.apache.http.HttpResponse;

import com.alibaba.fastjson.TypeReference;
import com.hm.common.network.httpclient.HttpClientFactory;
import com.hm.common.network.httpclient.HttpClientResponseParse;
import com.hm.common.su.bean.PageInfo;
import com.hm.common.su.bean.ServerResponse;

/**
 * @author shishun.wang
 * @date 2018年6月15日 下午4:09:25
 * @version 1.0
 * @describe
 */
public class Tesrt {

	public static void main(String[] args) throws Exception {

		for(int i = 0 ; i < 20 ;i++) {
			String uri ="http://127.0.0.1:10000/restful/v1.0/export/{0}/{1}?currentUserId={2}&dataSource={3}";
			HttpResponse response = HttpClientFactory.GET.build(uri, 1, 10, 260, "ERP").execute();
			ServerResponse<PageInfo<ExportFileRecordBean>> parse = HttpClientResponseParse.parseGeneric(response, new TypeReference<ServerResponse<PageInfo<ExportFileRecordBean>>>() {
			});
			PageInfo<ExportFileRecordBean> data = parse.getData();
			System.out.println(data.getContent().size());
			System.out.println("-------------------------------------------------");
		}
	}
}
