package util.test;

import java.util.UUID;

import com.hm.common.util.EncryptUtil.HMAC_SHA1;

/**
 * @author shishun.wang
 * @date 2018年4月24日 上午10:37:10
 * @version 1.0
 * @describe
 */
public class HMAC_SHA1Test {

	public static void main(String[] args) {

		String genHMAC = HMAC_SHA1.hmac(UUID.randomUUID().toString(), System.currentTimeMillis()+"");
		System.out.println(genHMAC.length()); // 28
		System.out.println(genHMAC); // O5fviq3DGCB5NrHcl/JP6+xxF6s=
	}
}
