package com.hm.jdk8.inf;

/**
 * @author shishun.wang
 * @date 2018年4月2日 下午2:24:55
 * @version 1.0
 * @describe
 */
public interface IMyInterface {

	default int init(int num) {
		System.out.println("构建接口默认方法");
		return num * 100 * 100;
	}

	void print();
}
