package com.hm.jdk8.inf;

/**
 * @author shishun.wang
 * @date 2018年4月2日 下午2:26:15
 * @version 1.0
 * @describe
 */
public class TestInf {

	public static void main(String[] args) {
		new IMyInterface() {

			@Override
			public void print() {
				System.out.println("获取初始化数据:" + this.init(15));
			}
		};
	}
}
