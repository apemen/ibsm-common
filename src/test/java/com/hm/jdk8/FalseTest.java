package com.hm.jdk8;

/**
 * @author shishun.wang
 * @date 2018年4月11日 上午11:32:39
 * @version 1.0
 * @describe 
 */
public class FalseTest {

	public static void main(String[] args) {
		
		if(false && false) {
			System.out.println("1");
		}
		
		if(false || false) {
			System.out.println("2");
		}
		
		if(true || false) {
			System.out.println("3");
		}
		
		if(true || true) {
			System.out.println("4");
		}
	}
}
