package com.hm.jdk8.stream;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @author shishun.wang
 * @date 2018年4月2日 下午3:35:27
 * @version 1.0
 * @describe
 */
public class SeniorMapCollectTest {

	public static void main(String[] args) {
		List<Person> persons = Arrays.asList(new Person("Max", 18), new Person("Peter", 23), new Person("Pamela", 23), new Person("David", 12));

		System.out.println("--按年龄分组----------");
		Map<Integer, List<Person>> map = persons.parallelStream().collect(Collectors.groupingBy(p -> p.age));
		System.out.println(map);
		map.forEach((key, value) -> {
			System.out.format("年龄为%s,的人有%s \n", key, value);
		});
		System.out.println("--计算平均年龄-----------------");
		Double age = persons.parallelStream().collect(Collectors.averagingInt(p -> p.age));
		System.out.println("平均年龄是" + age);

		System.out.println("--如果你对更多统计学方法感兴趣，概要收集器返回一个特殊的内置概要统计对象，所以我们可以简单计算最小年龄、最大年龄、算术平均年龄、总和和数量--");
		IntSummaryStatistics statistics = persons.parallelStream().collect(Collectors.summarizingInt(p -> p.age));
		System.out.println(statistics);

		System.out.println("--将所有人连接为一个字符串--------");
		String string = persons.parallelStream().filter(p -> p.age > 18).map(p -> p.name).collect(Collectors.joining(" 和 ", "(前缀)欢迎以下同学", "(后缀)年龄大于18岁"));
		System.out.println(string);

		/*
		 * 为了将数据流中的元素转换为映射，我们需要指定键和值如何被映射。要记住键必须是唯一的，否则会抛出IllegalStateException异常。
		 * 你可以选择传递一个合并函数作为额外的参数来避免这个异常。
		 * 
		 * 既然我们知道了一些最强大的内置收集器，让我们来尝试构建自己的特殊收集器吧。我们希望将流中的所有人转换为一个字符串，包含所有大写的名称，并以|分割。
		 * 为了完成它，我们通过Collector.of()创建了一个新的收集器。我们需要传递一个收集器的四个组成部分：供应器、累加器、组合器和终止器。
		 */

		Collector<Person, StringJoiner, String> collector =
			    Collector.of(
			        () -> new StringJoiner(" | "),          // supplier
			        (j, p) -> j.add(p.name.toUpperCase()),  // accumulator
			        (j1, j2) -> j1.merge(j2),               // combiner
			        StringJoiner::toString);                // finisher
		System.out.println(persons.parallelStream().collect(collector));
	}
}
