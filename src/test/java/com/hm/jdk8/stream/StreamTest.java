package com.hm.jdk8.stream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shishun.wang
 * @date 2018年4月2日 下午2:46:18
 * @version 1.0
 * @describe
 */
public class StreamTest {

	public static List<String> stringCollection = new ArrayList<String>();

	static {
		stringCollection.add("ddd2");
		stringCollection.add("aaa2");
		stringCollection.add("bbb1");
		stringCollection.add("aaa1");
		stringCollection.add("bbb3");
		stringCollection.add("ccc");
		stringCollection.add("bbb2");
		stringCollection.add("ddd1");
		stringCollection.add("bbb5");
	}

	public static void main(String[] args) {

		System.out.println("--filter--------------");
		stringCollection.parallelStream().filter(item -> item.equals("aaa2")).forEach(System.out::println);
		System.out.println("--Sorted(默认按自然顺序,从小到大)--------------");
		stringCollection.stream().filter(item -> item.startsWith("b")).sorted().forEach(System.out::println);
		System.out.println("--Sorted(变种)--------------");
		stringCollection.stream().filter(item -> item.startsWith("b")).sorted((a, b) -> a.compareTo(b)).forEach(System.out::println);
		System.out.println("--map--------------");
		stringCollection.parallelStream().map(item -> item.toUpperCase()).forEach(System.out::println);
		System.out.println("--Match(匹配操作有多种不同的类型，都是用来判断某一种规则是否与流对象相互吻合的。所有的匹配操作都是终结操作，只返回一个boolean类型的结果)--------------");
		System.out.println(stringCollection.parallelStream().anyMatch(item -> item.startsWith("cc")));
		System.out.println(stringCollection.parallelStream().allMatch(item -> item.startsWith("aa")));
		System.out.println(stringCollection.parallelStream().noneMatch(item -> item.startsWith("zzz")));
		System.out.println("--Count--------------");
		System.out.println(stringCollection.parallelStream().filter(item -> item.startsWith("aa")).count());
		System.out.println("--Reduce--------------");
		System.out.println(stringCollection.parallelStream().filter(item -> item.startsWith("aa")).reduce((s1, s2) -> s1 + "#" + s2).get());

		Map<Integer, String> map = new HashMap<Integer, String>();
		for (int i = 0; i < 5; i++) {
			map.putIfAbsent(i, "val" + i);
		}
		System.out.println("--map循环--------------");
		map.forEach((key, value) -> {
			System.out.println(key + ";" + value);
		});
		System.out.println(map.getOrDefault(15, "key为15的map数据没找到"));
		map.merge(4, "我是新值", (old, news) -> old.concat(news));
		System.out.println(map.get(4));

	}
}
