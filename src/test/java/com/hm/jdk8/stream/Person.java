package com.hm.jdk8.stream;

/**
 * @author shishun.wang
 * @date 2018年4月2日 下午3:34:43
 * @version 1.0
 * @describe
 */
public class Person {

	String name;
	int age;

	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return name;
	}
}
