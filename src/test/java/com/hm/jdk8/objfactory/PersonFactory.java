package com.hm.jdk8.objfactory;

/**
 * @author shishun.wang
 * @date 2018年4月2日 下午2:29:14
 * @version 1.0
 * @describe
 */
public interface PersonFactory<P extends Person> {

	P create();

	P create(String name, String age);
}
