package com.hm.jdk8.objfactory;

/**
 * @author shishun.wang
 * @date 2018年4月2日 下午2:28:23
 * @version 1.0
 * @describe
 */
public class Person {

	String name;

	String age;

	/**
	 * @param name
	 * @param age
	 */
	public Person(String name, String age) {
		super();
		this.name = name;
		this.age = age;
	}

	/**
	 * 
	 */
	public Person() {
		super();
	}

}
