package com.hm.jdk8.thread;

import java.util.concurrent.TimeUnit;

/**
 * @author shishun.wang
 * @date 2018年4月9日 下午4:43:00
 * @version 1.0
 * @describe
 */
public class ThreadTest {

	public static void main(String[] args) {

		Runnable runnable = () -> {

			try {
				TimeUnit.SECONDS.sleep(3);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("当前线程是-->" + Thread.currentThread().getName());
		};

		System.out.println("Strat");
		new Thread(runnable).start();

		System.out.println("Done");
	}
}
