package com.hm.jdk8.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author shishun.wang
 * @date 2018年4月9日 下午4:47:55
 * @version 1.0
 * @describe
 */
public class ExecutorServiceTest {

	public static void main(String[] args) throws Exception {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		try {
			executor.submit(() -> {
				System.out.println(Thread.currentThread().getName());
			});

			executor.shutdown();
			executor.awaitTermination(5, TimeUnit.SECONDS);
		} finally {
			if (!executor.isTerminated()) {
				System.out.println("线程还未执行完成");
			}
			executor.shutdownNow();
			System.out.println("线程立即停止");
		}
	}
}
