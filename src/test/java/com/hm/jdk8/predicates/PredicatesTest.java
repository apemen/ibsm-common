package com.hm.jdk8.predicates;

import java.util.function.Predicate;

/**
 * @author shishun.wang
 * @date 2018年4月2日 下午2:41:36
 * @version 1.0
 * @describe 
 */
public class PredicatesTest {

	public static void main(String[] args) {
		Predicate<String> predicate = (s) -> s.length() > 2;
		
		System.out.println(predicate.test("a3a"));
		System.out.println(predicate.negate().test("a2a"));
		
	}
}
