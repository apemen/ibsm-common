package com.hm.jdk8.format;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author shishun.wang
 * @date 2018年4月3日 下午6:13:09
 * @version 1.0
 * @describe
 */
public class DateUtil {

	// 线程不安全
	// private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd
	// HH:mm:ss");

	// 线程安全
	private static final ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>() {
		public SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}
	};

	public static String formatDate(Date date) throws ParseException {
		return sdf.get().format(date);
	}

	public static Date parse(String strDate) throws ParseException {

		return sdf.get().parse(strDate);
	}
}
