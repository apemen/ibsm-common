package com.hm.common.netty.su;

/**
 * @author shishun.wang
 * @date 2018年5月4日 上午11:12:15
 * @version 1.0
 * @describe
 */
public interface ServerCallBack {

	public String execute(String body);
}
