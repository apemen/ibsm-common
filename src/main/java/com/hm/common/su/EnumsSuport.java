package com.hm.common.su;

import java.util.Map;

/**
 * @author shishun.wang
 * @date 2018年6月2日 上午9:26:28
 * @version 1.0
 * @describe
 */
public interface EnumsSuport {

	public String code();

	public String desc();

	public int sort();
	
	public Map<String,String> enumValues();
}
