package com.hm.common.network.okhttp;

import java.util.Map;

import com.hm.common.util.CommonUtil;

import okhttp3.FormBody;
import okhttp3.FormBody.Builder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author shishun.wang
 * @date 2018年7月5日 下午3:04:48
 * @version 1.0
 * @describe
 */
public enum OkHttpFormFactory {

	POST {

		private OkHttpClient okHttpClient;

		private Request request;

		@Override
		public OkHttpFormFactory build(String url, Map<String, String> params) {
			okHttpClient = new OkHttpClient();

			okhttp3.Request.Builder reqBuilder = new Request.Builder().url(url);
			if (CommonUtil.isEmpty(params)) {
				request = reqBuilder.build();
				return this;
			}

			Builder builder = new FormBody.Builder();
			params.forEach((key, value) -> {
				builder.add(key, value);
			});
			request = reqBuilder.post(builder.build()).build();

			return this;
		}

		@Override
		public Response execute() throws Exception {
			return okHttpClient.newCall(request).execute();
		}

	};

	public abstract OkHttpFormFactory build(String url, Map<String, String> params);

	public abstract Response execute() throws Exception;
}
