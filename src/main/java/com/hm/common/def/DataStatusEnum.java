package com.hm.common.def;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hm.common.su.EnumsSuport;
import com.hm.common.util.StringUtil;

/**
 * @author shishun.wang
 * @date 下午10:41:28 2017年2月1日
 * @version 1.0
 * @describe 数据状态
 */
public enum DataStatusEnum implements EnumsSuport {

	/**
	 * 禁用（已停止使用）/已删除
	 */
	DISABLE("DISABLE", "已停止使用", 2),

	/**
	 * 启用（正在使用）
	 */
	ENABLE("ENABLE", "正在使用", 1);

	private String code;

	private String desc;

	private DataStatusEnum() {
	}

	private DataStatusEnum(String code, String desc, int sort) {
		this.code = code;
		this.desc = desc;
		this.sort = sort;
	}

	public String desc() {
		return this.desc;
	}

	private int sort;
	
	public int sort() {
		return this.sort;
	}

	public String code() {
		return this.code;
	}

	public static DataStatusEnum conversion(String code) {
		if (StringUtil.isBlankOrNull(code)) {
			return null;
		}
		for (DataStatusEnum statusDef : DataStatusEnum.values()) {
			if (statusDef.code().equals(code)) {
				return statusDef;
			}
		}
		return null;
	}

	public Map<String, String> enumValues() {
		Map<String, String> vo = new HashMap<String, String>();
		
		List<DataStatusEnum> vos = Arrays.asList(DataStatusEnum.values());
		Collections.sort(vos, Comparator.comparing(DataStatusEnum::sort));
		for (DataStatusEnum statusDef : vos) {
			vo.put(statusDef.code, statusDef.desc);
		}

		return vo;
	}
}
