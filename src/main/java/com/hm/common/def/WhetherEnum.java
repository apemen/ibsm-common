package com.hm.common.def;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hm.common.su.EnumsSuport;
import com.hm.common.util.StringUtil;

/**
 * @author shishun.wang
 * @date 下午5:32:02 2017年9月19日
 * @version 1.0
 * @describe
 */
public enum WhetherEnum implements EnumsSuport {

	YES("YES", "是", 2),

	NO("NO", "否", 1);

	private String code;

	private String desc;

	private int sort;

	private WhetherEnum() {
	}

	private WhetherEnum(String code, String desc, int sort) {
		this.code = code;
		this.desc = desc;
		this.sort = sort;
	}

	public int sort() {
		return this.sort;
	}

	public String desc() {
		return this.desc;
	}

	public String code() {
		return this.code;
	}

	public static WhetherEnum conversion(String code) {
		if (StringUtil.isBlankOrNull(code)) {
			return null;
		}
		for (WhetherEnum enumTmp : WhetherEnum.values()) {
			if (enumTmp.code.equals(code)) {
				return enumTmp;
			}
		}
		return null;
	}

	public Map<String, String> enumValues() {
		Map<String, String> vo = new HashMap<String, String>();
		
		List<WhetherEnum> vos = Arrays.asList(WhetherEnum.values());
		Collections.sort(vos, Comparator.comparing(WhetherEnum::sort));  
		for (WhetherEnum statusDef : vos) {
			vo.put(statusDef.code, statusDef.desc);
		}

		return vo;
	}
}
