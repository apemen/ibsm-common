package com.hm.common.def;

import com.hm.common.util.StringUtil;

/**
 * @author shishun.wang
 * @date 下午6:41:40 2017年2月2日
 * @version 1.0
 * @describe 激活状态
 */
public enum ActivateEnum {

	/**
	 * 禁用（未激活）
	 */
	DISABLE("DISABLE", "禁用（未激活）"),

	/**
	 * 启用（已激活）
	 */
	ENABLE("ENABLE", "启用（已激活）");

	private String code;

	private String desc;

	private ActivateEnum(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String desc() {
		return this.desc;
	}

	public String code() {
		return this.code;
	}
	
	public static ActivateEnum conversion(String code) {
		if (StringUtil.isBlankOrNull(code)) {
			return null;
		}
		for (ActivateEnum statusDef : ActivateEnum.values()) {
			if (statusDef.code().equals(code)) {
				return statusDef;
			}
		}
		return null;
	}
}
