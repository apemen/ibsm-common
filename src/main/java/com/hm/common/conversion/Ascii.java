package com.hm.common.conversion;

/**
 * @author shishun.wang
 * @date 2018年5月7日 下午3:07:39
 * @version 1.0
 * @describe
 */
public class Ascii {

	private String ascii;

	public Ascii(String ascii) {
		this.ascii = ascii;
	}

	public String toU16() {
		char[] chars = "0123456789ABCDEF".toCharArray();
		StringBuilder sb = new StringBuilder("");
		byte[] bs = ascii.getBytes();
		int bit;

		for (int i = 0; i < bs.length; i++) {
			bit = (bs[i] & 0x0f0) >> 4;
			sb.append(chars[bit]);
			bit = bs[i] & 0x0f;
			sb.append(chars[bit]);
			sb.append(' ');
		}
		return sb.toString().trim();
	}
}
